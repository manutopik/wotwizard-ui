# wotwizard-ui

## Prerequisite

This project needs NodeJS v16

## Contribute

```bash
$ git clone https://git.duniter.org/paidge/wotwizard-ui.git
$ cd wotwizard-ui
$ nvm use 16
$ git checkout -b my-branch
$ npm install
$ npm run dev
... Development...
$ npm run analyze
$ git commit
$ git push
```

Then create a merge request.

### Add a new page

Copy/paste the file `./pages/template.vue` and rename it to create a new page.

This template is extremly commented for beginners so you can create an apollo query and display the response very easily even using i18n !

If you want to add your page in the menu, edit the `menus` variable in the `./layouts/default.vue` file.

### GraphQL

All files concerning Apollo Graphql are stored in `./graphql`.

The schema documentation is stored in the `./graphql/doc/graphQLschema.txt` file.

In `queries.js` you'll find all queries.

If you want to add a 2nd graphQL server, edit `./graphql/clients/endpoints.js` to put your URL and uncomment the line concerning the `apollo.clientConfigs.myotherclient` option in `nuxt.config.js`. I have not tested this functionnality.

### Special Directories

For detailed explanation on how things work, check out the [documentation](https://nuxtjs.org).

You can create the following extra directories, some of which have special behaviors. Only `pages` is required; you can delete them if you don't want to use their functionality.

#### `assets`

The assets directory contains your uncompiled assets such as Stylus or Sass files, images, or fonts.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/assets).

#### `components`

The components directory contains your Vue.js components. Components make up the different parts of your page and can be reused and imported into your pages, layouts and even other components.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/components).

#### `layouts`

Layouts are a great help when you want to change the look and feel of your Nuxt app, whether you want to include a sidebar or have distinct layouts for mobile and desktop.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/layouts).

#### `pages`

This directory contains your application views and routes. Nuxt will read all the `*.vue` files inside this directory and setup Vue Router automatically.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/get-started/routing).

#### `plugins`

The plugins directory contains JavaScript plugins that you want to run before instantiating the root Vue.js Application. This is the place to add Vue plugins and to inject functions or constants. Every time you need to use `Vue.use()`, you should create a file in `plugins/` and add its path to plugins in `nuxt.config.js`.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/plugins).

#### `static`

This directory contains your static files. Each file inside this directory is mapped to `/`.

Example: `/static/robots.txt` is mapped as `/robots.txt`.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/static).

#### `store`

This directory contains your Vuex store files. Creating a file in this directory automatically activates Vuex.

More information about the usage of this directory in [the documentation](https://nuxtjs.org/docs/2.x/directory-structure/store).

## Build Setup

```bash
# use node 16
$ nvm use 16

# install dependencies
$ npm install

# build for production and launch server for Server Side Rendering (SSR)
$ npm run build
$ npm run start

# Or generate static project
$ npm run generates
```
