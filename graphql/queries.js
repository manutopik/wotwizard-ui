import gql from "graphql-tag"

// Pour la sidebar
export const LAST_BLOCK = gql`
	query LastBlock {
		countMax {
			number
			bct
			utc0
		}
	}
`

// Pour la page index
export const LAST_EVENTS = gql`
	query LastEvents($start: Int64, $end: Int64) {
		membersCount(start: $start, end: $end) {
			idList {
				__typename
				member: id {
					__typename
					pubkey
					uid
					status
					hash
					minDatePassed
					minDate
					limitDate
					history {
						__typename
						in
						block {
							__typename
							number
						}
					}
					received_certifications {
						__typename
						limit
					}
				}
				inOut
			}
			block {
				__typename
				number
			}
		}
	}
`

// Pour la page previsions/newcomers
export const NEWCOMERS = gql`
	query GetNewcomers {
		wwResult {
			__typename
			permutations_nb
			dossiers_nb
			certifs_nb
			forecastsByNames {
				__typename
				member: id {
					__typename
					pubkey
					uid
					status
					hash
					limitDate
					received_certifications {
						__typename
						limit
					}
				}
				date
				after
				proba
			}
		}
	}
`

// Pour la page membres/index
export const SEARCH_MEMBERS = gql`
	query SearchMember($hint: String) {
		idSearch(with: { hint: $hint }) {
			__typename
			ids {
				__typename
				pubkey
				uid
				status
				minDate
				minDatePassed
				hash
				limitDate
				received_certifications {
					__typename
					limit
				}
			}
		}
	}
`

// Pour la page membres/_hash
export const SEARCH_MEMBER = gql`
	query SearchMemberWithHash($hash: Hash!) {
		idFromHash(hash: $hash) {
			...attr
			pubkey
			isLeaving
			sentry
			membership_pending
			distanceE {
				__typename
				value {
					__typename
					ratio
				}
				dist_ok
			}
			distance {
				__typename
				value {
					__typename
					ratio
				}
				dist_ok
			}
			received_certifications {
				__typename
				certifications {
					__typename
					from {
						...attr
					}
					expires_on
					pending
				}
			}
			sent_certifications {
				__typename
				to {
					...attr
				}
				expires_on
				pending
			}
		}
	}
	fragment attr on Identity {
		__typename
		uid
		hash
		status
		limitDate
		minDate
		minDatePassed
		quality {
			__typename
			ratio
		}
		received_certifications {
			__typename
			limit
		}
	}
`

// Pour la page parametres
export const PARAMS = gql`
	query getParams {
		allParameters {
			name
			par_type
			value
			comment
		}
	}
`
// Pour la page favoris
export const FAVORIS = gql`
	query getFavoris($group: [String!]!) {
		filterGroup(group: $group) {
			__typename
			selected {
				__typename
				id {
					...attr_fav
				}
			}
			others {
				__typename
				id {
					...attr_fav
				}
			}
		}
	}
	fragment attr_fav on Identity {
		__typename
		pubkey
		uid
		status
		hash
		limitDate
		minDatePassed
		minDate
		received_certifications {
			__typename
			limit
		}
	}
`

// Pour la page index
export const NEXT_EXITS = gql`
	query NextExits($group: [String!], $start: Int64, $period: Int64) {
		memEnds(group: $group, startFromNow: $start, period: $period) {
			__typename
			pubkey
			uid
			status
			hash
			minDatePassed
			minDate
			limitDate
			received_certifications {
				__typename
				limit
			}
		}
	}
`
