import en from './locales/en.json'
import fr from './locales/fr.json'
import es from './locales/es.json'
import { dateTimeFormats } from './locales/dateTimeFormats'

export default {
  fallbackLocale: 'en',
  dateTimeFormats,
  messages: { en, fr, es }
}