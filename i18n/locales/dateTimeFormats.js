export const dateTimeFormats = {
    'fr': {
        short: {
            day: 'numeric',
            month: 'short',
            year: '2-digit'
        },
        long: {
            day: 'numeric',
            month: 'long',
            year: 'numeric',
        },
        hour: {
            hour: 'numeric'
        },
        min: {
            minute: '2-digit'
        },
        time: {
            hour: 'numeric',
            minute: '2-digit',
            hour12: false,
        }
    },
    'en': {
        short: {
            day: 'numeric',
            month: 'short',
            year: '2-digit'
        },
        long: {
            month: 'long',
            day: 'numeric',
            year: 'numeric',
        },
        hour: {
            hour: 'numeric'
        },
        min: {
            minute: '2-digit'
        },
        time: {
            hour: 'numeric',
            minute: '2-digit',
            hour12: true,
        }
    },
    'es': {
        short: {
            day: 'numeric',
            month: 'short',
            year: '2-digit'
        },
        long: {
            day: 'numeric',
            month: 'long',
            year: 'numeric',
        },
        hour: {
            hour: 'numeric'
        },
        min: {
            minute: '2-digit'
        },
        time: {
            hour: 'numeric',
            minute: '2-digit',
            hour12: false,
        }
    }
}