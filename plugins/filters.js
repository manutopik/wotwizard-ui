import Vue from "vue"

Vue.filter("dateStatus", (val) => {
	if (val == 0) return "info"
	const diff = val - Date.now() / 1000
	switch (true) {
		case diff < 0:
			return "danger"
		case diff < 2635200:
			return "warning"
		case diff >= 2635200:
			return "success"
	}
})
