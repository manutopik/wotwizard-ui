import Vue from "vue"

export default (context, inject) => {
	let liste_favoris = localStorage.favourites
		? JSON.parse(localStorage.favourites)
		: []

	let toggleFavourite = (uid, e) => {
		$(e.target).tooltip({
			title: function () {
				return context.$favourites.list.includes(uid)
					? context.app.i18n.t("favoris.supprime")
					: context.app.i18n.t("favoris.enregistre")
			},
			html: true,
			trigger: "manual"
		})

		$(e.target).tooltip("show")
		setTimeout(() => {
			$(e.target).tooltip("hide")
		}, 600)

		if (!context.$favourites.list.includes(uid)) {
			context.$favourites.list.push(uid)
		} else {
			context.$favourites.list = context.$favourites.list.filter(
				(item) => item !== uid
			)
		}

		localStorage.favourites = JSON.stringify(context.$favourites.list)
	}

	inject(
		"favourites",
		Vue.observable({
			list: liste_favoris,
			toggleFavourite: toggleFavourite
		})
	)
}
