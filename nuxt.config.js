import i18n from "./i18n"
import webpack from "webpack"

export default {
	// Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
	ssr: false,

	// Target: https://go.nuxtjs.dev/config-target
	target: "static",

	// Global page headers: https://go.nuxtjs.dev/config-head
	head: {
		title: "wotwizard-ui",
		meta: [
			{ charset: "utf-8" },
			{ name: "viewport", content: "width=device-width, initial-scale=1" },
			{ hid: "description", name: "description", content: "" },
			{ name: "format-detection", content: "telephone=no" }
		],
		link: [{ rel: "icon", type: "image/x-icon", href: "/favicon-96x96.png" }]
	},

	// Global CSS: https://go.nuxtjs.dev/config-css
	css: ["@/assets/css/style.scss"],

	// Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
	plugins: [
		"~plugins/favourites.js",
		"~plugins/filters.js",
		"~plugins/bootstrap.js"
	],

	// Auto import components: https://go.nuxtjs.dev/config-components
	components: true,

	// Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
	buildModules: [
		// https://go.nuxtjs.dev/pwa
		"@nuxtjs/pwa"
	],

	// Modules: https://go.nuxtjs.dev/config-modules
	modules: [
		// https://github.com/nuxt-community/apollo-module
		"@nuxtjs/apollo",
		// https://i18n.nuxtjs.org
		"@nuxtjs/i18n"
	],

	i18n: {
		defaultLocale: "fr",
		locales: [
			{
				code: "en",
				name: "English"
			},
			{
				code: "fr",
				name: "Français"
			},
			{
				code: "es",
				name: "Español"
			}
		],
		detectBrowserLanguage: {
			alwaysRedirect: true
		},
		vueI18n: i18n
	},

	// PWA module configuration: https://go.nuxtjs.dev/pwa
	pwa: {
		icon: {
			purpose: "any"
		},
		meta: {
			name: "Wotwizard",
			author: "Paidge",
			description:
				"Vérifiez les entrées et les sorties de la toile de confiance de la monnaie libre Ğ1",
			lang: "fr",
			ogHost: "wotwizard.axiom-team.fr",
			ogImage: true,
			twitterCard: "app",
			nativeUI: true,
			theme_color: "#343a40"
		},
		manifest: {
			name: "Wotwizard",
			short_name: "Wotwizard",
			description:
				"Vérifiez les entrées et les sorties de la toile de confiance de la monnaie libre Ğ1",
			lang: "fr",
			background_color: "#343a40",
			shortcuts: [
				{ name: "Favoris", url: "/favoris" },
				{ name: "Futurs membres", url: "/previsions" },
				{ name: "Lexique", url: "/lexique" },
				{ name: "Accueil", url: "/" }
			]
		}
	},

	apollo: {
		clientConfigs: {
			default: "~/graphql/clients/coindufeu"
			//you can setup multiple clients with arbitrary names
			//, myotherclient : '~/graphql/clients/otherclient'
		}
	},

	router: {
		linkExactActiveClass: "active"
	},

	// Build Configuration: https://go.nuxtjs.dev/config-build
	build: {
		plugins: [
			new webpack.ProvidePlugin({
				$: "jquery"
			})
		]
	}
}
